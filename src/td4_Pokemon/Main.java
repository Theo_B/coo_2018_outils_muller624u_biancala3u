package td4_Pokemon;
public class Main {
	public static void main(String[] args) {
		Arborescence arbre = new Arborescence();

		int pokemon = arbre.ajouteDansRepertoire("Pokemon", 0);

		int famillebulbizarre = arbre.ajouteDansRepertoire("Famille Bulbizarre", pokemon);
		int bulbizarre = arbre.ajouteDansRepertoire("Bulbizarre", famillebulbizarre);
		int herbizarre = arbre.ajouteDansRepertoire("Herbizare", famillebulbizarre);
		int florizarre = arbre.ajouteDansRepertoire("Florizarre", famillebulbizarre);

		int Famsalameche = arbre.ajouteDansRepertoire("Famille Salameche", pokemon);
		int salameche = arbre.ajouteDansRepertoire("Salameche", Famsalameche);
		int reptincel = arbre.ajouteDansRepertoire("Reptincel", Famsalameche);
		int dracaufeu = arbre.ajouteDansRepertoire("Dracaufeu", Famsalameche);

		int famPikachu = arbre.ajouteDansRepertoire("Famille Pikachu", pokemon);
		int pikachu = arbre.ajouteDansRepertoire("Pikachu", famPikachu);
		int raichu = arbre.ajouteDansRepertoire("Raichu", famPikachu);

		int personnage = arbre.ajouteDansRepertoire("Personnage", 0);
		int sacha = arbre.ajouteDansRepertoire("sacha", personnage);
		int iris = arbre.ajouteDansRepertoire("iris", personnage);

		int rocket = arbre.ajouteDansRepertoire("rocketTeam", personnage);
		int jessie = arbre.ajouteDansRepertoire("jessie", rocket);
		int james = arbre.ajouteDansRepertoire("james", rocket);
		
		System.out.println(arbre);
		System.out.println(arbre.getCheminAbsolu(jessie));
		System.out.println(arbre.getCheminAbsolu(pikachu));
		System.out.println(arbre.getCheminAbsolu(pokemon));
		
		
		System.out.println(arbre.getCheminAbsolu("rocketTeam"));
		
		System.out.println(arbre.parentCommun(rocket, sacha));
		System.out.println(arbre.parentCommun(rocket, pokemon));
		System.out.println(arbre.parentCommun(herbizarre, bulbizarre));
		
		System.out.println("** de herbizarre � bulbizarre");
		System.out.println(arbre.parentCommun(herbizarre, bulbizarre));
		System.out.println(arbre.getCheminRelatif(herbizarre, bulbizarre));
		
		System.out.println("** de pokemon a jessin");
		System.out.println(arbre.getCheminRelatif(pokemon, jessie));
		
	}
}
