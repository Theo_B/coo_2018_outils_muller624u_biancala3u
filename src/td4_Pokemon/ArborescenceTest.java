package td4_Pokemon;
import static org.junit.Assert.*;

import org.junit.Test;
import org.junit.Before;


public class ArborescenceTest {

	Arborescence arbre;
	
	@Before
	public void creationDonnees()
	{
		this.arbre=new Arborescence();
		
		int pokemon=this.arbre.ajouteDansRepertoire("pokemon",0);
		
		int famillebulbizarre=this.arbre.ajouteDansRepertoire("famillebulbizarre",pokemon);
		int bulbizarre=this.arbre.ajouteDansRepertoire("Bulbizarre",famillebulbizarre);
		int herbizarre=this.arbre.ajouteDansRepertoire("Herbizare",famillebulbizarre);
		int florizarre=this.arbre.ajouteDansRepertoire("Florizarre",famillebulbizarre);
		
		int Famsalameche=this.arbre.ajouteDansRepertoire("famillesalameche",pokemon);
		int salameche=this.arbre.ajouteDansRepertoire("Salameche",Famsalameche);
		int reptincel=this.arbre.ajouteDansRepertoire("Reptincel",Famsalameche);
		int dracaufeu=this.arbre.ajouteDansRepertoire("Dracaufeu",Famsalameche);
		
		int famPikachu=this.arbre.ajouteDansRepertoire("famillepikachu",pokemon);
		int pikachu=this.arbre.ajouteDansRepertoire("Pikachu",famPikachu);
		int raichu=this.arbre.ajouteDansRepertoire("Raichu",famPikachu);
		

		int personnage=this.arbre.ajouteDansRepertoire("personnage",0);
		int sacha=this.arbre.ajouteDansRepertoire("sacha",personnage);
		int iris=this.arbre.ajouteDansRepertoire("iris",personnage);
		
		int rocket=this.arbre.ajouteDansRepertoire("rocketTeam",personnage);
		int jessie=this.arbre.ajouteDansRepertoire("jessie",rocket);
		int james=this.arbre.ajouteDansRepertoire("james",rocket);

	
		
	}
	
	
	@Test
	public void test() {
		fail("Not yet implemented");
	}

}
