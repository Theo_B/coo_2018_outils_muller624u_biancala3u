package td4_Pokemon;
import java.util.ArrayList;

/**
 * represente une structure de repertoire
 * 
 */
public class Arborescence {

	/**
	 * les elements contenus dans cette arborescence
	 */
	private ArrayList<Element> elements;

	/**
	 * permet de construire une arborescence vide
	 */
	public Arborescence() {
		this.elements = new ArrayList<Element>();
		// ajoute l'element racine sans parent
		this.elements.add(new Element("racine", -1));
	}

	/**
	 * ajoute un element dans un repertoire parent
	 * 
	 * @return id de l'element ajoute
	 */
	public int ajouteDansRepertoire(String nom, int idParent) {
		// on verifie que l'id parent existe bien
		if (idParent >= this.elements.size())
			// sinon retourne -1
			return (-1);

		// on ajoute l'element dans la liste en fils de l'element parent
		this.elements.add(new Element(nom, idParent));
		return this.elements.size() - 1;
	}

	/**
	 * affiche l'arborescence
	 */
	public String toString() {
		String r = "";
		for (int i = 0; i < this.elements.size(); i++)
			r += i + "->" + this.elements.get(i) + "\n";
		return (r);
	}

	/**
	 * nom complet � partir d'un id
	 * 
	 * @param id
	 *            id dont on veut le nom d'acc�s
	 */
	public String getCheminAbsolu(int id) {
		// TODO Etudiant
		throw new Error("a completer");

	}

	/**
	 * nom complet � partir d'un nom
	 * 
	 * @param nom
	 *            nom dont on veut le nom d'acc�s
	 */
	public String getCheminAbsolu(String nom) {
		// TODO Etudiant
		throw new Error("a completer");
	}

	/**
	 * trouver le parent commun
	 * 
	 * @param id1
	 *            id de l'element 1
	 * @param id2
	 *            id de l'element 2
	 * @return le repertoire parent commun
	 */
	public String parentCommun(int id1, int id2) {
		// TODO Etudiant
		throw new Error("a completer");

	}

	/**
	 * retourne le chemin relatif pour passer de id1 a id2
	 * @param id1
	 * @param id2
	 * @return
	 */
	public String getCheminRelatif(int id1, int id2) {
		// TODO Etudiant
		throw new Error("a completer");

	}

}
