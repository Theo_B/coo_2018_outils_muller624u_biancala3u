package td4_Pokemon;
/**
 * un element represente un repertoire ou un fichier
 *
 */
public class Element {

	/**
	 * idParent correspond � l'ID du repertoire parent
	 */
	private int idParent;

	/**
	 * nom de l'element
	 */
	private String nom;

	/**
	 * construit un nouvel element
	 * 
	 * @param nom
	 *            nom de l'element
	 * @param parent
	 *            id du parent de l'element
	 */
	public Element(String nom, int parent) {
		this.nom = nom;
		this.idParent = parent;
	}
	
	/**
	 * @return le nom de l'element 
	 */
	public String getNom()
	{
		return this.nom;
	}
	
	/**
	 * @return id du repertoire parent
	 */
	public int getIDParent()
	{
		return(this.idParent);
	}
	
	public String toString()
	{
		return (this.nom)+" up: "+this.idParent;
	}
}
