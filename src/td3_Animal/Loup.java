package td3_Animal;

public class Loup implements Animal{
int pv;
int stockNourriture;
	
	public Loup(){
		pv =50;
		stockNourriture=20;
	}
	@Override
	public boolean etreMort() {
		boolean res=false;
		if(pv==0){
		res=true;	
		}
		return res;
		
	}

	@Override
	public boolean passerUnjour() {
		boolean res = false;
		if(stockNourriture>=5){
			stockNourriture=stockNourriture-5;
			res=true;
		}
		else{
			if(pv>=11){
				pv=pv-10;
				res=true;
			}
			else{
				pv=0;
			}

		}
		return res;
	}

	@Override
	public void stockerNourriture(int nourriture) {
		stockNourriture=stockNourriture+nourriture;
		
	}

	@Override
	public int getPv() {

		return pv;
	}

	@Override
	public int getStockNourriture() {
		return stockNourriture;
	}

	
	
}
