
package td3_Animal;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;
public class TestLoup{
	Loup l;
	@Test
	@Before
	public void setup(){
		l=new Loup();
	}
	@Test
	public void testConstructeurLoup() {
		assertEquals("les pv devrait etre de 50", 50,l.getPv());
		assertEquals("le stock de nourriture devrait etre de 20",20, l.getStockNourriture());
	}
	@Test
	public void testStockerNourriture() {
		l.stockerNourriture(10);
		assertEquals("le stock de nourriture devrait etre de 30", 30, l.getStockNourriture());
	}
	@Test
	public void testStockerNourriture_nega() {
		l.stockerNourriture(-40);
		assertEquals("le stock de nourriture devrait etre de -20", -20, l.getStockNourriture());
	}
	@Test
	public void testPasserJour_t_Mort_f() {
		boolean res=l.passerUnjour();
		assertEquals("le jour devrait du passer", true,res);
		assertEquals("le loup devrait etre en vie", false,l.etreMort());
	}
	@Test
	public void testPasserJour_f_Mort_t() throws Exception {
		for(int i=0;i<10;i++){
			l.passerUnjour();
		}
		boolean res=l.passerUnjour();
		assertEquals("le jour ne devrait pas passer", false,res);
		assertEquals("le loup devrait etre Mort", true,l.etreMort());
	}
	

}

