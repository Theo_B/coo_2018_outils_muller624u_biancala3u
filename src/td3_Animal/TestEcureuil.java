package td3_Animal;

import static org.junit.Assert.*;

import org.junit.Test;

public class TestEcureuil {
	@Test
	
	public void testConstructeur(){
		Ecureuil e = new Ecureuil();
		
		assertEquals("Les pv devrait �tre de 10", 10, e.getPv());
		assertEquals( "le stock devrait etre de 5",5, e.getStockNourriture());
	}
	
	@Test
	public void testpasserUnJour(){
		Ecureuil e1 = new Ecureuil();
		Ecureuil e2 = new Ecureuil();
		
		e1.passerUnjour();
		
		for (int i = 0; i < 500; i++) {
			e2.passerUnjour();
		}
		assertEquals("Les pv devrait �tre de 10", 10, e1.getPv());
		assertEquals( "le stock devrait etre de 4",4, e1.getStockNourriture());
		
		assertEquals("Les pv devrait �tre de 0", 0, e2.getPv());
		assertEquals( "le stock devrait etre de 0",0, e2.getStockNourriture());
		
	}
	
	@Test
	public void testEtreMort(){
		Ecureuil e2 = new Ecureuil();
		for (int i = 0; i < 500; i++) {
			e2.passerUnjour();
		}

		
		assertEquals("Les pv devrait �tre de 0", 0, e2.getPv());
		assertEquals( "le stock devrait etre de 0",0, e2.getStockNourriture());
	}
	
	@Test
	public void TestStockerNourriture() {
		Ecureuil e = new Ecureuil();
		e.stockerNourriture(5);
		
		assertEquals("Le stock de nourriture devrait �tre de 10",10, e.getStockNourriture());
	}
}
