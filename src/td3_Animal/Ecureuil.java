package td3_Animal;

public class Ecureuil implements Animal{
	int pv;
	int nourriture;
	public Ecureuil()  {
		this.pv=10;
		this.nourriture=5;
	}

	@Override
	public boolean etreMort() {
		// TODO Auto-generated method stub
		if(this.pv==0){
			return true;
		}
		return false;
	}

	@Override
	public boolean passerUnjour() {
		// TODO Auto-generated method stub
		if(!etreMort()){
			if(this.nourriture > 0){
				this.nourriture=this.nourriture-1;
				return true;
			}else{
				this.pv=this.pv-1;
				if(etreMort()){
					return false;
				}else{
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public void stockerNourriture(int nourriture) {
		// TODO Auto-generated method stub
		this.nourriture = this.nourriture + nourriture;
	}

	@Override
	public int getPv() {
		// TODO Auto-generated method stub
		return this.pv;
	}

	@Override
	public int getStockNourriture() {
		// TODO Auto-generated method stub
		return this.nourriture;
	}


}
